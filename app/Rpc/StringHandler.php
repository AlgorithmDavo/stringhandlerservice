<?php


namespace App\Rpc;

use gitkv\GearmanRpc\HandlerContract;
use App\Repositories\StringHandler\StringHandlerRepository;

class StringHandler implements HandlerContract
{
    /**
     * @var $stringHandlerRepository StringHandlerRepository.
     */
    private $stringHandlerRepository;

    public function handle($payload)
    {
        $updatedText = '';
        foreach ($payload['job']['methods'] as $method) {
            switch ($method) {
                case "stripTags":
                    app()->bind("App\Repositories\StringHandler\StringHandlerRepository", "App\Repositories\StringHandler\StripTagsService");
                    break;
                case "replaceSpacesToEol":
                    app()->bind("App\Repositories\StringHandler\StringHandlerRepository", "App\Repositories\StringHandler\ReplaceSpacesToEolService");
                    break;
                case "removeSpaces":
                    app()->bind("App\Repositories\StringHandler\StringHandlerRepository", "App\Repositories\StringHandler\RemoveSpacesService");
                    break;
                case "htmlspecialchars":
                    app()->bind("App\Repositories\StringHandler\StringHandlerRepository", "App\Repositories\StringHandler\HtmlSpecialCharsService");
                    break;
                case "removeSymbols":
                    app()->bind("App\Repositories\StringHandler\StringHandlerRepository", "App\Repositories\StringHandler\RemoveSymbolsService");
                    break;
                case "toNumber":
                    app()->bind("App\Repositories\StringHandler\StringHandlerRepository", "App\Repositories\StringHandler\ToNumberService");
                    break;
            }
            $this->stringHandlerRepository = app(StringHandlerRepository::class);
            $text = (!empty($updatedText)) ?  $updatedText : $payload['job']['text'];
            $updatedText = $this->stringHandlerRepository->processData($text);
        }

        return ['text' => $updatedText];
    }
}
