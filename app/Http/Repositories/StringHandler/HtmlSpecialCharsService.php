<?php

namespace App\Repositories\StringHandler;

class HtmlSpecialCharsService implements StringHandlerRepository
{
    /**
     * replace symbols with html entities.
     *
     * @param string $text
     * @return string
     */
    public function processData(string $text): string
    {
        return htmlspecialchars($text);
    }
}
