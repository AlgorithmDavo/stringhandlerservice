<?php

namespace App\Repositories\StringHandler;

interface StringHandlerRepository
{
    public function processData(string $text): string;
}
