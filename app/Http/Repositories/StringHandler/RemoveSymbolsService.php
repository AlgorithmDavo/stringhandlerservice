<?php

namespace App\Repositories\StringHandler;

class RemoveSymbolsService implements StringHandlerRepository
{
    /**
     * remove given symbols from text.
     *
     * @param string $text
     * @return string
     */
    public function processData(string $text): string
    {
        return preg_replace('[.,/!@#$%^&*()]', '', $text);
    }
}
