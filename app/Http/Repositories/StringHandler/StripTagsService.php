<?php

namespace App\Repositories\StringHandler;

class StripTagsService implements StringHandlerRepository
{
    /**
     * Strip tags from given text
     *
     * @param string $text
     * @return string
     */
    public function processData(string $text): string
    {
        return strip_tags($text);
    }
}
