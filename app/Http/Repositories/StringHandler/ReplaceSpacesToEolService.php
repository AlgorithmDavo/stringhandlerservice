<?php

namespace App\Repositories\StringHandler;

class ReplaceSpacesToEolService implements StringHandlerRepository
{
    /**
     * replace space with eol.
     *
     * @param string $text
     * @return string
     */
    public function processData(string $text): string
    {
        return str_replace(' ', "\n", $text);
    }
}
