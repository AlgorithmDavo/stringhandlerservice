<?php

namespace App\Repositories\StringHandler;

class RemoveSpacesService implements StringHandlerRepository
{
    /**
     * Removing spaces from given string.
     *
     * @param string $text
     * @return string
     */
    public function processData(string $text): string
    {
        return preg_replace('/\s+/', '', $text);
    }
}
