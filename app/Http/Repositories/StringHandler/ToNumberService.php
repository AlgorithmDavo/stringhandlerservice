<?php

namespace App\Repositories\StringHandler;

class ToNumberService implements StringHandlerRepository
{
    /**
     * convert text to string
     *
     * @param string $method
     * @return string
     */
    public function processData(string $text): string
    {
        return intval(preg_replace('/[^0-9]+/', '', $text), 10);
    }
}
