<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GearmanRpc;

class ProcessController extends Controller
{

    /**
     * Process the string by request methods.
     *
     * @param  Request $request
     * @return Response
     */
    public function processData(Request $request)
    {
        return GearmanRpc::doNormal('StringHandler', $request->getContent());
    }
}
