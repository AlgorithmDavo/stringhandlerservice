<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($request->IsJson()) {
            if ($e instanceof MethodNotAllowedHttpException) {
                $code = Response::HTTP_METHOD_NOT_ALLOWED;
                $e = new MethodNotAllowedHttpException([], 'Method not allowed', $e);
                $message = $e->getMessage();
            } elseif ($e instanceof NotFoundHttpException) {
                $code = Response::HTTP_NOT_FOUND;
                $e = new NotFoundHttpException('Resource not found', $e);
                $message = $e->getMessage();
            }
            elseif ($e instanceof ValidationException) {
                $rendered =  parent::render($request,$e);
                $code = $rendered->getStatusCode();
                $message = $rendered->original;
            }
            return response()->json([
                'error' => [
                    'code' => $code,
                    'message' => $message
                ]
            ], $code);
        }
        else {
            return parent::render($request,$e);
        }
    }
}
