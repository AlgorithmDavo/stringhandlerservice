<?php

class StringHandlerTest extends TestCase
{

    /**
     *  / [GET]
     */
    public function testApplication()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    /**
     *  / [POST]
     */
    public function testShouldStripTags()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ['stripTags']
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }


    /**
     *  / [POST]
     */
    public function testShouldReplaceSpacesToEol()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ['replaceSpacesToEol']
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }


    /**
     *  / [POST]
     */
    public function testShouldReplaceRemoveSpaces()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ['removeSpaces']
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }

    /**
     *  / [POST]
     */
    public function testShouldHandleHtmlSpecialChars()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ['htmlspecialchars']
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }

    /**
     *  / [POST]
     */
    public function testShouldRemoveSymbols()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ['removeSymbols']
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }

    /**
     *  / [POST]
     */
    public function testShouldHandleToNumber()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ['toNumber']
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }

    /**
     *  / [POST]
     */
    public function testShouldHandleAllTogether()
    {
        $data = [
            'job' => [
                'text' => "Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!" ,
                'methods' =>  ["stripTags", "removeSpaces", "replaceSpacesToEol", "htmlspecialchars", "removeSymbols", "toNumber"]
            ]
        ];

        $this->json('POST', '/job', $data , ['CONTENT_TYPE' => 'application/json']);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'text'
        ]);
    }
}
